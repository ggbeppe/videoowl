const express = require('express');
const connectDB = require('./config/db');
const morgan = require('morgan');

const app = express();

// Connect to Database
connectDB();

// Initialize middleware
app.use(express.json({ extended: false }));
app.use(morgan('combined'));

// Test route
app.get('/', (req, res) => res.send('API running'));

// Routes
app.use('/api/users', require('./routes/users'));
app.use('/api/auth', require('./routes/auth'));

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`);
});
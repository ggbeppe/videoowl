const mongoose = require('mongoose');

const ProjectSchema = mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
    },
    name: {
        type: String,
        required: true
    },
    description: {
        type: String
    }
});

module.exports = Project = mongoose.model('project', ProjectSchema);
const mongoose = require('mongoose');

const ItemSchema = mongoose.Schema({
    project: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'project'
    },
    name: {
        type: String,
        required: true
    },
    url: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    tags: {
        type: [String]
    }
});

module.exports = Item = mongoose.model('item', ItemSchema);

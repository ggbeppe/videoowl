const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator/check');
const auth = require('../middleware/auth');

const Project = require('../models/Project');
const User = require('../models/User');

// @route   POST /api/projects/
// @desc    Create a new project
// @access  Private
router.post('/', [auth, [
    check('name', 'Please a name for your project').not().isEmpty()
]], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    try {
        const newProject = Project({
            name: req.body.name,
            user: req.user.id
        });

        const project = await newProject.save();
        res.json({ project });

    } catch (e) {
        console.error(e.message);
        return res.status(500).json({ msg: 'Server Error' });
    }
});

// @route   GET /api/projects/
// @desc    Get all projects
// @access  Private
router.get('/', auth, async (req, res) => {
    try {
        const projects = await Project.find({ user: req.user.id });
        return res.json({ projects })

    } catch (e) {
        console.error(e.message);
        return res.status(500).json({ msg: 'Server Error' });
    }
});
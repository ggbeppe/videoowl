const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');
const { check, validationResult } = require('express-validator/check');
const auth = require('../middleware/auth');

const User = require('../models/User');

// @route   POST /api/auth/login
// @desc    Login user and get token
// @access  Public
router.post('/login', [
    check('email', 'Please include a valid email').isEmail(),
    check('password', 'Password is required').exists()
], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    const { email, password } = req.body;

    try {
        let user = await User.findOne({ email });

        if (!user) {
            return res.status(400).json({ errors: [ { msg: 'Invalid Credentials' } ] });
        }

        const isMatch = await bcrypt.compare(password, user.password);

        if (!isMatch) {
            return res.status(400).json({ errors: [ { msg: 'Invalid Credentials' } ] });
        }

        const payload = {
            user: {
                id: user.id
            }
        };

        jwt.sign(payload, config.get('jwtSecret'), { expiresIn: config.get('tokenExpLength')}, (err, token) => {
            if (err) {
                throw err;
            } else {
                return res.json({ token });
            }
        });

    } catch (e) {
        console.error(e.message);
        res.status(500).json({ msg: 'Server Error' })
    }
});

// @route   POST /api/auth/status
// @desc    Get user status
// @access  Private
router.get('/status', auth, async (req, res) => {
    try {
        const user = await User.findById(req.user.id).select('-password');
        return res.json({ user });
    } catch (e) {
        console.error(e.message);
        res.status(500).json({ msg: 'Server Error' });
    }
});

module.exports = router;